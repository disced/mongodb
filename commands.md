# Basic

`> db` : show database in use.

`> show dbs` : show all databases.

`> use db_name` : use a data base or create new db.


# Basic Structure

```mermaid
graph RL;
  a[(DB)] --> Collections;
  Collections --> Documents;

```

